#! /usr/bin/python3 

import numpy as np

"""This script will perform the optimisation of the post-retirement withdrawal.
"""

"""
    Problem: Maximise the annual amount withdrawn from Super

    Constraints: 
        1) Superfund = $500 000
        2) Savings = $1 000 000
        3) Taxation is applied at the marginal tax rate
        4) Assume no capital gains tax
        5) Preservation age of 60 (the age at which you can access Super)
        6) Age Pension begins at 67 (this is currently conservative)
        7) Super contribution is 12.5%
        8) Income is $75 000 - gross ??? HOLD THIS OFF FOR NOW

    Alternative scenarios
        1) Age Pension is 70 yrs old
"""

ageVec = np.array()
