# TODO
- Reorganise the code base for a more pythonic project
 - This means include the setup.py, init.py, subfolders
- Improve the post-retirement draw down. This will need to include some information about taxation of the withdrawal. Perhaps an optimisation function - maximising the annual return over a estimated life expectancy. I think this could be achieved
- figure out the best way to structure the modules
- Acorns updates to remove from Expenses -> savings
